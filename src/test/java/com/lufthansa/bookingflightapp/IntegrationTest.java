package com.lufthansa.bookingflightapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class IntegrationTest {

    @Autowired
    private TestDb testDb;

    public TestDb getTestDb() {
        return testDb;
    }
}
