package com.lufthansa.bookingflightapp.user;

import com.lufthansa.bookingflightapp.DataGeneratorUtil;
import com.lufthansa.bookingflightapp.IntegrationTest;
import com.lufthansa.bookingflightapp.request.UserParamsRequest;
import com.lufthansa.bookingflightapp.response.UserSaveDto;
import com.lufthansa.bookingflightapp.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UsersTests extends IntegrationTest {
    @Autowired
    private UserService userService;

    @Test
    void saveUserSuccessfully(){
        var user= (UserSaveDto)userService.save(UserParamsRequest.builder()
                .name(DataGeneratorUtil.generateAlphaValue(10))
                .surname(DataGeneratorUtil.generateAlphaValue(10))
                .username(DataGeneratorUtil.generateAlphaValue(10))
                .password(DataGeneratorUtil.generateAlphaValue(10))
                .build());
        var expectedUser= getTestDb().findUser(user.getUserId());
        Assertions.assertTrue(expectedUser.isPresent());
        Assertions.assertEquals(expectedUser.get().getUserId(),user.getUserId());
    }




}
