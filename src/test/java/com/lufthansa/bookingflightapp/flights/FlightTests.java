package com.lufthansa.bookingflightapp.flights;

import com.lufthansa.bookingflightapp.DataGeneratorUtil;
import com.lufthansa.bookingflightapp.IntegrationTest;
import com.lufthansa.bookingflightapp.request.FlightParamsRequest;
import com.lufthansa.bookingflightapp.response.FlightDto;
import com.lufthansa.bookingflightapp.service.FlightService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.time.LocalTime;


@SpringBootTest
class FlightTests extends IntegrationTest {

    @Autowired
    private FlightService flightService;

    @Test
    void saveFlightSuccessfully() {
        var flight = (FlightDto) flightService.save(FlightParamsRequest.builder()
                .travelingClass(DataGeneratorUtil.generateAlphaValue(1))
                .destination(DataGeneratorUtil.generateAlphaNumericValue(10))
                .departure(DataGeneratorUtil.generateAlphaNumericValue(10))
                .departureTime(LocalTime.now())
                .date(LocalDate.now())
                .build());
        var expectedFlight = getTestDb().findFlight(flight.getFlightId());
        Assertions.assertTrue(expectedFlight.isPresent());
        Assertions.assertEquals(expectedFlight.get().getFlightId(),flight.getFlightId());
        Assertions.assertEquals(expectedFlight.get().getDeparture(),flight.getDeparture());
    }


    @Test
    void updateFlightSuccessfully(){
       var flight= getTestDb().insertFlight(1L);
       var newDestination=  DataGeneratorUtil.generateAlphaNumericValue(10);
       var flightParamsRequest= FlightParamsRequest.builder()
               .date(flight.getDate())
               .departureTime(flight.getDepartureTime())
               .departure(flight.getDeparture())
               .destination(newDestination)
               .travelingClass(flight.getTravelingClass())
               .build();
        var expectedFlight= (FlightDto)flightService.update(flight.getFlightId(), flightParamsRequest);
        Assertions.assertEquals(expectedFlight.getDestination(), newDestination);
    }


    @Test
    void deleteFlightSuccessfully(){
        var flight= getTestDb().insertFlight(1L);
        var flightId= flight.getFlightId();
        flightService.delete(flightId);
        var expectedFlight = getTestDb().findFlight(flight.getFlightId());
        Assertions.assertFalse(expectedFlight.isPresent());

    }
   @Test
    void readFlightSuccessfully(){
       getTestDb().insertFlight(1L);
       getTestDb().insertFlight(2L);
       var expectedFlightList= flightService.read();
       Assertions.assertEquals(expectedFlightList.size(),2);

    }

    @Test
    void filterFlightsSuccessfully(){
        var flight= getTestDb().insertFlight(1L);
        var expectedFilterFlightsList= flightService.filterFlights(flight.getDeparture(), flight.getDestination(),
                flight.getDate(), flight.getTravelingClass(),0, 1);
        Assertions.assertEquals(expectedFilterFlightsList.size(),1);
    }

}
