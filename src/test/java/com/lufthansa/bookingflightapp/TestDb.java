package com.lufthansa.bookingflightapp;

import com.lufthansa.bookingflightapp.entity.FlightEntity;
import com.lufthansa.bookingflightapp.entity.RoleEntity;
import com.lufthansa.bookingflightapp.entity.UserEntity;
import com.lufthansa.bookingflightapp.repository.FlightRepository;
import com.lufthansa.bookingflightapp.repository.RoleRepository;
import com.lufthansa.bookingflightapp.repository.UserRepository;
import com.lufthansa.bookingflightapp.response.UserSaveDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.management.relation.Role;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

@Component
public class TestDb {

    @Autowired
    private FlightRepository flightRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;

    public FlightEntity insertFlight(long id) {
        FlightEntity flightEntity = FlightEntity.builder()
                .flightId(id)
                .departure(DataGeneratorUtil.generateAlphaValue(10))
                .destination(DataGeneratorUtil.generateAlphaValue(10))
                .departureTime(LocalTime.now())
                .date(LocalDate.now())
                .travelingClass(DataGeneratorUtil.generateAlphaValue(1))
                .build();
        return this.flightRepository.save(flightEntity);
    }

    public Optional<FlightEntity> findFlight(Long flightId) {
        return flightRepository.findById(flightId);
    }

    public Optional<UserEntity> findUser(Long userId) {
        return userRepository.findById(userId);
    }
}
