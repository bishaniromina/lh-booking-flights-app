package com.lufthansa.bookingflightapp;

import org.apache.commons.lang3.RandomStringUtils;

public class DataGeneratorUtil {

    public static String generateAlphaValue(int length) {
        return RandomStringUtils.random(length, true, false);
    }

    public static String generateAlphaNumericValue(int length) {
        return RandomStringUtils.random(length, true, true);
    }
}
