package com.lufthansa.bookingflightapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookingFlightAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookingFlightAppApplication.class, args);
	}

}
