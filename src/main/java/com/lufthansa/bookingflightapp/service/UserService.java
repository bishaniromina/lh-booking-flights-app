package com.lufthansa.bookingflightapp.service;

import com.lufthansa.bookingflightapp.request.UserParamsRequest;
import com.lufthansa.bookingflightapp.response.UserSaveDto;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

public interface UserService{

    UserDetails loadUserByUsername(String username);

    UserSaveDto save(UserParamsRequest userParamsRequest);

    UserSaveDto update(UserParamsRequest userParamsRequest);

    void delete(long id);

    List<UserSaveDto> read();
}
