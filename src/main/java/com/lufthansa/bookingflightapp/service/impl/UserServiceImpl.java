package com.lufthansa.bookingflightapp.service.impl;

import com.lufthansa.bookingflightapp.configs.CurrentUserProvider;
import com.lufthansa.bookingflightapp.entity.UserEntity;
import com.lufthansa.bookingflightapp.exception.NotFoundException;
import com.lufthansa.bookingflightapp.repository.UserRepository;
import com.lufthansa.bookingflightapp.request.UserParamsRequest;
import com.lufthansa.bookingflightapp.response.UserSaveDto;
import com.lufthansa.bookingflightapp.service.UserService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Log4j2
public class UserServiceImpl implements UserService, UserDetailsService {

    @Autowired
    public UserRepository userRepository;

    @Autowired
    public CurrentUserProvider currentUserProvider;

    @Override
    public UserSaveDto save(UserParamsRequest userParamsRequest) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedPassword = passwordEncoder.encode(userParamsRequest.getPassword());
        UserEntity userEntity = UserEntity.builder()
                .name(userParamsRequest.getName())
                .surname(userParamsRequest.getName())
                .username(userParamsRequest.getUsername())
                .password(hashedPassword)
                .build();
        userRepository.save(userEntity);
        log.info("User saved successfully!");
        return UserSaveDto.builder().userId(userEntity.getUserId())
                .name(userEntity.getName())
                .surname(userEntity.getSurname())
                .username(userEntity.getUsername())
                .password(hashedPassword)
                .build();

    }


    @Override
    public UserSaveDto update(UserParamsRequest userParamsRequest) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedPassword = passwordEncoder.encode(userParamsRequest.getPassword());
        var currentUser = currentUserProvider.getCurrentUser();
        UserEntity userEntity = userRepository.findById(currentUser.getId()).orElseThrow(() -> new NotFoundException("User id not valid"));
        userEntity.setName(userParamsRequest.getName());
        userEntity.setSurname(userParamsRequest.getSurname());
        userEntity.setUsername(userParamsRequest.getUsername());
        userEntity.setPassword(hashedPassword);
        userRepository.save(userEntity);
        log.info("User updated successfully!");
        return UserSaveDto.builder().userId(userEntity.getUserId())
                .name(userEntity.getName())
                .surname(userEntity.getSurname())
                .username(userEntity.getUsername())
                .password(userEntity.getPassword()).build();

    }

    @Override
    public void delete(long id) {
        userRepository.deleteById(id);
        log.info("User with id " + id + " is deleted successfully!");
    }

    @Override
    public List<UserSaveDto> read() {
        List<UserEntity> userEntityList = userRepository.findAll();
        return userEntityList.stream().map(userEntity -> UserSaveDto.builder()
                .name(userEntity.getName())
                .surname(userEntity.getSurname())
                .username(userEntity.getUsername())
                .build()

        ).collect(Collectors.toList());
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<UserEntity> userEntity = userRepository.findByUsername(username);
        if (userEntity.isEmpty()) {
            log.error("User with username " + username + " not found!");
            throw new UsernameNotFoundException(username);
        }
        var authorities = userEntity.get().getRoleEntityList().stream().map(role -> new SimpleGrantedAuthority(role.getAuthority())).collect(Collectors.toList());
        return new User(
                userEntity.get().getUsername(), userEntity.get().getPassword(), authorities);
    }

}
