package com.lufthansa.bookingflightapp.service.impl;

import com.lufthansa.bookingflightapp.configs.CurrentUserProvider;
import com.lufthansa.bookingflightapp.entity.FlightEntity;
import com.lufthansa.bookingflightapp.entity.FlightRequestEntity;
import com.lufthansa.bookingflightapp.entity.UserEntity;
import com.lufthansa.bookingflightapp.enums.FlightStatus;
import com.lufthansa.bookingflightapp.exception.NotFoundException;
import com.lufthansa.bookingflightapp.repository.FlightRepository;
import com.lufthansa.bookingflightapp.repository.FlightRequestRepository;
import com.lufthansa.bookingflightapp.repository.UserRepository;
import com.lufthansa.bookingflightapp.response.*;
import com.lufthansa.bookingflightapp.service.FlightRequestService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Log4j2
public class FlightRequestServiceImpl implements FlightRequestService {

    @Autowired
    private FlightRequestRepository flightRequestRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FlightRepository flightRepository;

    @Autowired
    private CurrentUserProvider currentUserProvider;


    @Override
    public FlightRequestDto requestFlight(Long flightId) {
        var currentUser = currentUserProvider.getCurrentUser();
        UserEntity userEntity = userRepository.findById(currentUser.getId()).orElseThrow(() -> new NotFoundException("User id not valid"));
        FlightEntity flightEntity = flightRepository.findById(flightId).orElseThrow(() -> new NotFoundException("Flight id not valid"));
        FlightRequestEntity flightRequestEntity = FlightRequestEntity.builder()
                .date(LocalDate.now())
                .userEntity(userEntity)
                .flightEntity(flightEntity)
                .requestStatues(FlightStatus.PENDING)
                .build();
        flightRequestRepository.save(flightRequestEntity);
        log.info("Flight request saved successfully!");
        return FlightRequestDto.builder()
                .flightRequestId(flightRequestEntity.getFlightRequestId())
                .build();
    }

    @Override
    public void statusUpdate(Long flightRequestId, FlightStatus status, String reason) {
        FlightRequestEntity flightRequestEntity = flightRequestRepository.findById(flightRequestId)
                .orElseThrow(() -> new NotFoundException("Flight request id not valid"));
        flightRequestEntity.setRequestStatues(status);
        flightRequestEntity.setReason(reason);
        flightRequestRepository.save(flightRequestEntity);
        log.info("Flight request updated successfully!");

    }

    @Override
    public List<RecentFlightRequestDto> orderRecentFlightRequest() {
        List<FlightRequestEntity> flightRequestEntityList = flightRequestRepository.findTop5ByOrderByDateDesc();
        return flightRequestEntityList.stream().map(flightRequestEntity -> RecentFlightRequestDto.builder()
                        .user(UserDto.builder()
                                .userId(flightRequestEntity.getUserEntity().getUserId())
                                .name(flightRequestEntity.getUserEntity().getName())
                                .surname(flightRequestEntity.getUserEntity().getSurname())
                                .build())
                        .flight(FlightDto.builder()
                                .flightId(flightRequestEntity.getFlightEntity().getFlightId())
                                .date(flightRequestEntity.getFlightEntity().getDate())
                                .departure(flightRequestEntity.getFlightEntity().getDeparture())
                                .departureTime(flightRequestEntity.getFlightEntity().getDepartureTime())
                                .destination(flightRequestEntity.getFlightEntity().getDestination())
                                .travelingClass(flightRequestEntity.getFlightEntity().getTravelingClass()).build())

                        .build())
                .collect(Collectors.toList());


    }

    @Override
    public FlightRequestDto requestFlightForAUser(Long userId, Long flightId) {
        UserEntity userEntity = userRepository.findById(userId).orElseThrow(() -> new NotFoundException("User id not valid"));
        FlightEntity flightEntity = flightRepository.findById(flightId).orElseThrow(() -> new NotFoundException("Flight id not valid"));
        FlightRequestEntity flightRequestEntity = FlightRequestEntity.builder()
                .userEntity(userEntity)
                .flightEntity(flightEntity)
                .requestStatues(FlightStatus.ACCEPTED)
                .date(LocalDate.now())
                .build();
        flightRequestRepository.save(flightRequestEntity);
        log.info("Flight request created successfully by Admin!");
        return FlightRequestDto.builder()
                .flightRequestId(flightRequestEntity.getFlightRequestId())
                .build();
    }

    @Override
    public List<RecentFlightRequestDto> orderRecentBookedFlights() {
        var currentUser = currentUserProvider.getCurrentUser();
        List<FlightRequestEntity> flightRequestEntityList = flightRequestRepository.findTop5ByUserEntityUserIdOrderByDateDesc(currentUser.getId());
        return flightRequestEntityList.stream().map(flightRequestEntity -> RecentFlightRequestDto.builder()
                        .flight(FlightDto.builder()
                                .flightId(flightRequestEntity.getFlightEntity().getFlightId())
                                .departure(flightRequestEntity.getFlightEntity().getDeparture())
                                .departureTime(flightRequestEntity.getFlightEntity().getDepartureTime())
                                .destination(flightRequestEntity.getFlightEntity().getDestination())
                                .travelingClass(flightRequestEntity.getFlightEntity().getTravelingClass()).build())
                        .user(UserDto.builder().name(flightRequestEntity.getUserEntity().getName())
                                .userId(flightRequestEntity.getUserEntity().getUserId())
                                .surname(flightRequestEntity.getUserEntity().getSurname())
                                .build())
                        .build())

                .collect(Collectors.toList());

    }


    @Override
    public void delete(long id) {
        flightRequestRepository.deleteById(id);
        log.info("Flight request with id " + id + " is deleted successfully!");
    }


}
