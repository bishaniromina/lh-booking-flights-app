package com.lufthansa.bookingflightapp.service.impl;

import com.lufthansa.bookingflightapp.entity.FlightEntity;
import com.lufthansa.bookingflightapp.exception.NotFoundException;
import com.lufthansa.bookingflightapp.repository.FlightRepository;
import com.lufthansa.bookingflightapp.request.FlightParamsRequest;
import com.lufthansa.bookingflightapp.response.FlightDto;
import com.lufthansa.bookingflightapp.service.FlightService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.sql.SQLIntegrityConstraintViolationException;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Log4j2
public class FlightServiceImpl implements FlightService{

    @Autowired
    private FlightRepository flightRepository;

    @Override
    public FlightDto save(FlightParamsRequest flightParamsRequest) {
        FlightEntity flightEntity = FlightEntity.builder()
                .departure(flightParamsRequest.getDeparture())
                .destination(flightParamsRequest.getDestination())
                .departureTime(flightParamsRequest.getDepartureTime())
                .date(flightParamsRequest.getDate())
                .travelingClass(flightParamsRequest.getTravelingClass())
                .build();
        flightRepository.save(flightEntity);
        log.info("Flight saved successfully!");
        return FlightDto.builder()
                .flightId(flightEntity.getFlightId())
                .departure(flightEntity.getDeparture())
                .destination(flightEntity.destination)
                .date(flightEntity.date)
                .travelingClass(flightEntity.getTravelingClass())
                .departureTime(flightEntity.getDepartureTime())
                .build();
    }

    @Override
    public FlightDto update(long id, FlightParamsRequest flightParamsRequest) {
        FlightEntity flightEntity = flightRepository.findById(id).orElseThrow(() -> new NotFoundException("Flight id not valid"));
        flightEntity.setDestination(flightParamsRequest.getDestination());
        flightEntity.setDeparture(flightParamsRequest.getDeparture());
        flightEntity.setDepartureTime(flightParamsRequest.getDepartureTime());
        flightEntity.setDate(flightParamsRequest.getDate());
        flightEntity.setTravelingClass(flightParamsRequest.getTravelingClass());
        flightRepository.save(flightEntity);
        log.info("Flight updated successfully!");
        return FlightDto.builder()
                .flightId(flightEntity.getFlightId())
                .destination(flightEntity.getDestination())
                .departure(flightEntity.getDeparture())
                .departureTime(flightEntity.getDepartureTime())
                .date(flightEntity.getDate())
                .travelingClass(flightEntity.getTravelingClass())
                .build();
    }

    @Override
    public void delete(long id) {

        flightRepository.deleteById(id);
        log.info("Flight with id " + id + " is deleted successfully!");
    }

    @Override
    public List<FlightDto> read() {
        List<FlightEntity> flightEntityList = flightRepository.findAll();
        return flightEntityList.stream().map(flightEntity -> FlightDto.builder()
                .destination(flightEntity.getDestination())
                .departure(flightEntity.getDeparture())
                .departureTime(flightEntity.getDepartureTime())
                .date(flightEntity.getDate())
                .travelingClass(flightEntity.getTravelingClass())
                .build()
        ).collect(Collectors.toList());
    }

    @Override
    public List<FlightDto> filterFlights(String departure, String destination, LocalDate date, String travellingClass, int pageNo, int pageSize) {
        Pageable paging = PageRequest.of(pageNo, pageSize);
        List<FlightEntity> flightEntityList = flightRepository.findByDepartureAndDestinationAndDateAndTravelingClass(departure, destination, date, travellingClass, paging);
        return flightEntityList.stream().map(flightEntity -> FlightDto.builder()
                .flightId(flightEntity.getFlightId())
                .departure(flightEntity.getDeparture())
                .destination(flightEntity.getDestination())
                .date(flightEntity.getDate())
                .departureTime(flightEntity.getDepartureTime())
                .travelingClass(flightEntity.getTravelingClass())
                .build())
                .collect(Collectors.toList());

    }
}
