package com.lufthansa.bookingflightapp.service;

import com.lufthansa.bookingflightapp.enums.FlightStatus;
import com.lufthansa.bookingflightapp.response.FlightRequestDto;
import com.lufthansa.bookingflightapp.response.RecentFlightRequestDto;

import java.util.List;

public interface FlightRequestService{

    FlightRequestDto requestFlight(Long flightId);

    void statusUpdate(Long flightRequestId, FlightStatus status, String reason);

    List<RecentFlightRequestDto> orderRecentFlightRequest();

    FlightRequestDto requestFlightForAUser(Long userId, Long flightId);

    List<RecentFlightRequestDto>orderRecentBookedFlights();

    void delete(long id);




}
