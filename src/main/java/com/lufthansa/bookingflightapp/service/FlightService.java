package com.lufthansa.bookingflightapp.service;

import com.lufthansa.bookingflightapp.request.FlightParamsRequest;
import com.lufthansa.bookingflightapp.response.FlightDto;

import java.time.LocalDate;
import java.util.List;


public interface FlightService{

    List<FlightDto>filterFlights(String departure, String destination, LocalDate date, String travellingClass, int pageNo, int pageSize);

    FlightDto save(FlightParamsRequest flightParamsRequest);

    FlightDto update(long id, FlightParamsRequest flightParamsRequest);

    void delete(long id);

    List<FlightDto> read();

}
