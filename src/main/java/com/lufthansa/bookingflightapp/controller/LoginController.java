package com.lufthansa.bookingflightapp.controller;

import com.lufthansa.bookingflightapp.configs.JwtUtil;
import com.lufthansa.bookingflightapp.request.AuthRequest;
import com.lufthansa.bookingflightapp.response.AuthResponseDto;
import com.lufthansa.bookingflightapp.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class LoginController {
    @Autowired
    private final AuthenticationManager authenticationManager;

    private final UserService userDetailsImplService;

    @Autowired
    private final JwtUtil jwtUtil;

    @PostMapping("/login")
    public ResponseEntity<?> createAuthToken(@RequestBody AuthRequest authRequest) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword())
        );

        final String token = jwtUtil.generateToken(authRequest.getUsername());

        return ResponseEntity.ok(new AuthResponseDto(token));
    }




}
