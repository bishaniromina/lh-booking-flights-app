package com.lufthansa.bookingflightapp.controller;

import com.lufthansa.bookingflightapp.request.UserParamsRequest;
import com.lufthansa.bookingflightapp.response.UserSaveDto;
import com.lufthansa.bookingflightapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    public UserService userService;

    @PostMapping("/createUser")
    public ResponseEntity<UserSaveDto> createUser(@RequestBody UserParamsRequest userParamsRequest){
        UserSaveDto userDto= (UserSaveDto) userService.save(userParamsRequest);
        return ResponseEntity.ok(userDto);
     }

     @PutMapping("/updateUser")
     public ResponseEntity<UserSaveDto> updateUser( @RequestBody UserParamsRequest userParamsRequest ){
       UserSaveDto userDto= (UserSaveDto) userService.update(userParamsRequest);
       return ResponseEntity.ok(userDto);
     }

     @DeleteMapping("/deleteUser/{userId}")
     public void deleteUser(@PathVariable Long userId){
        userService.delete(userId);
     }

     @GetMapping("getListOfUsers")
     public ResponseEntity<List<UserSaveDto>> getListOfUser(){
         List<UserSaveDto> userSaveDtoList= userService.read();
         return ResponseEntity.ok(userSaveDtoList);
     }





}
