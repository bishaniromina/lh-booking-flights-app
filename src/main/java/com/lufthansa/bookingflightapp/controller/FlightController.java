package com.lufthansa.bookingflightapp.controller;

import com.lufthansa.bookingflightapp.request.FlightParamsRequest;
import com.lufthansa.bookingflightapp.response.FlightDto;
import com.lufthansa.bookingflightapp.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api")
public class FlightController {
    @Autowired
    private FlightService flightService;

    @GetMapping("/getFilterFlights")
    public ResponseEntity<List<FlightDto>>getFilterFlights(@RequestParam String departure, @RequestParam String destination,
                                                           @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date,
                                                           @RequestParam String travellingClass,
                                                           @RequestParam int pageNo,
                                                           @RequestParam int pageSize
    ){
        List<FlightDto>flightDtoList= flightService.filterFlights(departure,destination,date,travellingClass, pageNo, pageSize);
        return ResponseEntity.ok(flightDtoList);
    }

    @DeleteMapping("/deleteFlight/{flightId}")
    public void deleteFlight(@PathVariable Long flightId){
        flightService.delete(flightId);
    }


    @PutMapping("/updateFlight/{flightId}")
    public ResponseEntity<FlightDto> updateFlight(@RequestBody FlightParamsRequest flightParamsRequest, @PathVariable Long flightId) {
        FlightDto flightDto= (FlightDto) flightService.update(flightId,flightParamsRequest);
        return ResponseEntity.ok(flightDto);
    }

    @PostMapping("/createFlight")
    public ResponseEntity<FlightDto> createFlight(@RequestBody FlightParamsRequest flightParamsRequest){
        FlightDto flightDto= (FlightDto) flightService.save(flightParamsRequest);
        return ResponseEntity.ok(flightDto);
    }


    @GetMapping("/getFlights")
    public ResponseEntity<List<FlightDto>> getListOfFlights(){
        List<FlightDto>flightDtoList=flightService.read();
        return ResponseEntity.ok(flightDtoList);
    }




}
