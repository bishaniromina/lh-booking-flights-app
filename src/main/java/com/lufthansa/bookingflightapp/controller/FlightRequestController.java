package com.lufthansa.bookingflightapp.controller;

import com.lufthansa.bookingflightapp.enums.FlightStatus;
import com.lufthansa.bookingflightapp.response.FlightRequestDto;
import com.lufthansa.bookingflightapp.response.RecentFlightRequestDto;
import com.lufthansa.bookingflightapp.service.FlightRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class FlightRequestController {

    @Autowired
    private FlightRequestService flightRequestService;

    @PostMapping("/requestFlight")
    public ResponseEntity<FlightRequestDto> requestFlight(@RequestParam Long flightId) {
        FlightRequestDto flightRequestDto = flightRequestService.requestFlight(flightId);
        return ResponseEntity.ok(flightRequestDto);
    }

    @DeleteMapping("/cancelFlight/{flightRequestId}")
    public void cancelFlight(@PathVariable Long flightRequestId) {
        flightRequestService.delete(flightRequestId);
    }

    @PutMapping("/statusUpdate/{flightRequestId}")
    public void statusUpdate(@PathVariable Long flightRequestId, @RequestParam FlightStatus status, @RequestParam String reason) {
        flightRequestService.statusUpdate(flightRequestId, status, reason);
    }

    @PostMapping("/requestFlightForUser")
    public ResponseEntity<FlightRequestDto> requestFlightForUser(@RequestParam Long userId, @RequestParam Long flightId) {
        FlightRequestDto flightRequestDto = flightRequestService.requestFlightForAUser(userId, flightId);
        return ResponseEntity.ok(flightRequestDto);
    }

    @GetMapping("/get5RecentFlightRequest")
    public ResponseEntity<List<RecentFlightRequestDto>> get5RecentFlightRequest() {
        List<RecentFlightRequestDto> recentFlightRequestDto = flightRequestService.orderRecentFlightRequest();
        return ResponseEntity.ok(recentFlightRequestDto);
    }

    @GetMapping("/get5RecentBookedFlights")
    public ResponseEntity<List<RecentFlightRequestDto>> get5RecentUsersBookedFlights() {
        List<RecentFlightRequestDto> recentFlightRequestDto = flightRequestService.orderRecentBookedFlights();
        return ResponseEntity.ok(recentFlightRequestDto);
    }


}
