package com.lufthansa.bookingflightapp.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FlightRequestParamRequest {

    private Long flightId;
    private Long userId;

}
