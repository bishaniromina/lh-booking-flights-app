package com.lufthansa.bookingflightapp.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@AllArgsConstructor
@Builder
@Data
public class AuthRequest {
    private String username;
    private String password;
}
