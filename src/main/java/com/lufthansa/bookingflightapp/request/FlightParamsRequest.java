package com.lufthansa.bookingflightapp.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FlightParamsRequest {
    public String departure;
    public String destination;
    public LocalTime departureTime;
    public String travelingClass;
    public LocalDate date;



}
