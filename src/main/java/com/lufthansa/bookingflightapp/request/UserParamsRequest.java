package com.lufthansa.bookingflightapp.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserParamsRequest {
    private String name;
    private String surname;
    private String username;
    private String password;


}
