package com.lufthansa.bookingflightapp.repository;

import com.lufthansa.bookingflightapp.entity.FlightRequestEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FlightRequestRepository extends JpaRepository<FlightRequestEntity, Long> {


    List<FlightRequestEntity>findTop5ByOrderByDateDesc();



    List<FlightRequestEntity>findTop5ByUserEntityUserIdOrderByDateDesc(Long userId);



}
