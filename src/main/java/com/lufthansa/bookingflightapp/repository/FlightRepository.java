package com.lufthansa.bookingflightapp.repository;

import com.lufthansa.bookingflightapp.entity.FlightEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface FlightRepository extends JpaRepository<FlightEntity,Long> {

   List<FlightEntity> findByDepartureAndDestinationAndDateAndTravelingClass(String departure, String destination, LocalDate date,
                                                                            String travellingClass, Pageable pageable);

}
