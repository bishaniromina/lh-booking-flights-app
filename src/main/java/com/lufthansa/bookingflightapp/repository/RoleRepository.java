package com.lufthansa.bookingflightapp.repository;

import com.lufthansa.bookingflightapp.entity.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<RoleEntity, Long> {
}
