package com.lufthansa.bookingflightapp.exception;

import javax.naming.AuthenticationException;

public class UserNotFoundException extends AuthenticationException {


    public UserNotFoundException(String msg) {
        super(msg);
    }

}
