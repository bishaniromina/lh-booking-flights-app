package com.lufthansa.bookingflightapp.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@AllArgsConstructor
@Builder
@Data
public class AuthResponseDto {
    private String token;
}
