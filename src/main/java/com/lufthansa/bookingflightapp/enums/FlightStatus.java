package com.lufthansa.bookingflightapp.enums;

public enum FlightStatus {

    PENDING, ACCEPTED, REJECTED
}
