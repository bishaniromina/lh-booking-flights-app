package com.lufthansa.bookingflightapp.configs;

import com.lufthansa.bookingflightapp.service.impl.UserServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final UserServiceImpl userDetailsImplService;

    @Autowired
    private AuthenticationFilter authenticationFilter;


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests()
                .antMatchers("/api/login").permitAll()
                .antMatchers("/api/createUser").permitAll()
                .antMatchers("/api/updateUser/**").permitAll()
                .antMatchers("/api/getFilterFlights").permitAll()
                .antMatchers("/api/swagger-resources/**", "/webjars/**", "/v3/**", "/swagger-ui.html/**","/v2/api-docs").permitAll()
                .antMatchers("/api/requestFlight").hasRole("USER")
                .antMatchers("/api/get5RecentFlightRequest").hasRole("ADMIN")
                .antMatchers("/api/get5RecentBookedFlights").hasRole("USER")
                .antMatchers("/api/statusUpdate/**").hasRole("ADMIN")
                .antMatchers("/api/cancelFlight/**").hasRole("USER")
                .antMatchers("/api/requestFlightForUser").hasRole("ADMIN")
                .antMatchers("/api/deleteFlight/**").hasRole("ADMIN")
                .antMatchers("/api/updateFlight/**").hasRole("ADMIN")
                .antMatchers("/api/createFlight").hasRole("ADMIN")
                .antMatchers("/api/getFlights").permitAll()
                .antMatchers("/api/deleteUser/**").hasRole("ADMIN")
                .antMatchers("/api/getListOfUsers").permitAll()
                .anyRequest().authenticated()
                .and().exceptionHandling().accessDeniedPage("/403").and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.addFilterBefore(authenticationFilter, UsernamePasswordAuthenticationFilter.class);
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsImplService);
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
