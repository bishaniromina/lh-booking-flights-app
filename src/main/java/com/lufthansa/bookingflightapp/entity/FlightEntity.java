package com.lufthansa.bookingflightapp.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "flight")
public class FlightEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long flightId;
    public String departure;
    public String destination;
    public LocalTime departureTime;
    public String travelingClass;
    public LocalDate date;


    @OneToMany(mappedBy = "flightEntity",cascade = CascadeType.REMOVE)
    private List<FlightRequestEntity> flightRequestEntityList;

}
