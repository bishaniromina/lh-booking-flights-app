package com.lufthansa.bookingflightapp.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Table(name = "users")
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long userId;
    public String name;
    public String surname;
    public String username;
    public String password;


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user-role",
            joinColumns = { @JoinColumn(name = "userId") },
            inverseJoinColumns = { @JoinColumn(name = "roleId") })
    private List<RoleEntity> roleEntityList;


    @OneToMany(mappedBy = "userEntity", cascade = CascadeType.ALL)
    private List<FlightRequestEntity> flightRequestEntityList;
}
