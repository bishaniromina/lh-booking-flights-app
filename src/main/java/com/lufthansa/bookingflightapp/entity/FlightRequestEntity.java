package com.lufthansa.bookingflightapp.entity;

import com.lufthansa.bookingflightapp.enums.FlightStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "flightRequest")
public class FlightRequestEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long flightRequestId;
    @Enumerated(EnumType.STRING)
    public FlightStatus requestStatues;
    public String reason;
    public LocalDate date;



    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId")
    private UserEntity userEntity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "flightId")
    private FlightEntity flightEntity;


}
