package com.lufthansa.bookingflightapp.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Table(name = "role")
public class RoleEntity implements GrantedAuthority {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long roleId;
    public String role;



    @ManyToMany(mappedBy = "roleEntityList")
    private List<UserEntity> userEntityList;


    @Override
    public String getAuthority() {
        return role;
    }
}
